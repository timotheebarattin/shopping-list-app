import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  loadedFeature: string = 'recipes';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyAcsZ7g2lynS65fH4y3YWHxSFVH-NyXugE",
      authDomain: "ng-recipe-book-6c8aa.firebaseapp.com"
    })
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
