import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Recipe } from '../recipes/recipe.model';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class DataStorageService {
    constructor(private http: HttpClient, private authService: AuthService) {}

    saveRecipes(recipes: Recipe[]) {
        const token = this.authService.getToken();

        return this.http.put('https://ng-recipe-book-6c8aa.firebaseio.com/recipes.json?auth=' + token, recipes);
    }

    fetchRecipes() {
        const token = this.authService.getToken();

        return this.http.get('https://ng-recipe-book-6c8aa.firebaseio.com/recipes.json?auth=' + token);
        //   .pipe(map(
        //     (response) => {
        //         const data = response.json();
        //         return data;
        //     }
        // ))
    }
}
