import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Recipe } from '../recipe.model'

import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
})
export class RecipeListComponent implements OnInit, OnDestroy {
  // @Output() onDetail = new EventEmitter<Recipe>();
  
  // recipes: Recipe[] = [
  //   new Recipe('A Test Recipe', 'This is only a test', 'https://static.pexels.com/photos/691114/pexels-photo-691114.jpeg'),
  //   new Recipe('Another Test Recipe', 'This is only a test', 'https://static.pexels.com/photos/691114/pexels-photo-691114.jpeg')
  // ];
  recipes: Recipe[] = [];
  subscription: Subscription;


  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.subscription = this.recipeService.recipesChanged.subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
      }
    );
    this.recipes = this.recipeService.getRecipes();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe;
  }

}
