import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Recipe } from '../recipe.model';

import { ShoppingListService } from '../../shopping-list/shopping-list.service';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipeIndex: number;
  recipe: Recipe;

  constructor(
    private shoppingListService: ShoppingListService, 
    private recipeService: RecipeService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    // this.recipeIndex = this.route.snapshot['index'];
    // this.recipe = this.recipeService.getRecipe(this.recipeIndex);

    this.route.params.subscribe(
      (params: Params) => {
        this.recipeIndex = +params['index'];
        this.recipe = this.recipeService.getRecipe(this.recipeIndex);
      }
    )
  }

  recipeToSL() {
    for (let i=0; i<this.recipe.ingredients.length; i++) {
      this.shoppingListService.addIngredient(this.recipe.ingredients[i]);
    }
    // this.shoppingListService.addIngredient(...this.recipe.ingredients)
  }

  onDelete() {
    this.recipeService.deleteRecipe(this.recipeIndex);
  }

}
