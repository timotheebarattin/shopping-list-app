import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { DataStorageService } from '../shared/data-storage.service';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model'
 
@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe(
            'Peanut Butter Cookies', 
            'Crunchy peanut butter cookies that will get your butt fat and juicy!', 
            'https://c1.staticflickr.com/3/2625/4000565959_1f84105999_b.jpg',
            [new Ingredient('Flour', 1/2), new Ingredient('Peanut Butter', 1), new Ingredient('Butter', 1)]
        ),
        new Recipe(
            'Foccaccia Italiana', 
            'Italian-style thick pizza that will have you lick your figertips', 
            'https://www.silviocicchi.com/pizzachef/wp-content/uploads/2015/03/p26.jpg',
            [new Ingredient('Flour', 1), new Ingredient('Olive Oil', 1), new Ingredient('Cherry Tomatoes', 15)]
        ),
    ];

    constructor(private dataStorageService: DataStorageService) {}

    saveRecipes() {
        this.dataStorageService.saveRecipes(this.recipes).subscribe(
            (response) => {
                console.log("Recipes saved");
                console.log(this.recipes)},
            (error) => console.log("Shite, something went wrong!")
        )
    }

    fetchRecipes() {
        this.dataStorageService.fetchRecipes().subscribe(
            (recipes: Recipe[]) => {
                this.recipes = recipes;
                console.log(this.recipes)},
            (error) => console.log("Shite, something went wrong!")
        )
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }

}